const express = require("express");
const orderRoute = express.Router();

const { createOrder } = require("../controller/orderController");

orderRoute.post("/createOrder", createOrder);

module.exports = orderRoute;
