const express = require("express");
const rootRoute = express.Router();
const userRoute = require("./userRoute");
const foodRoute = require("./foodRoute");
const restaurantRoute = require("./restaurantRoute");
const likeRoute = require("./likeRoute");
const rateRoute = require("./rateRoute");
const orderRoute = require("./orderRoute");

rootRoute.use("/user", userRoute);
rootRoute.use("/food", foodRoute);
rootRoute.use("/restaurant", restaurantRoute);
rootRoute.use("", likeRoute);
rootRoute.use("", rateRoute);
rootRoute.use("", orderRoute);

module.exports = rootRoute;
