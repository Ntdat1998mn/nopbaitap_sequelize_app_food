// localhost/api/user/version/method

const express = require("express");
const userRoute = express.Router();

// import commonjs Module
const { getUser } = require("../controller/userController");
const { getUserId } = require("../controller/userController");
const { createUser } = require("../controller/userController");
const { updateUser } = require("../controller/userController");

// Tao API phuong thuc GET
userRoute.get("/getUser", getUser);
userRoute.get("/getUserId/:id", getUserId);

// Create method POST
userRoute.post("/createUser", createUser);
// Create method PUT
userRoute.put("/updateUser/:id", updateUser);

module.exports = userRoute;
