const express = require("express");
const likeRoute = express.Router();

// import commonjs Module
const { like } = require("../controller/likeController");
const { unLike } = require("../controller/likeController");

// Tao API handleLike
likeRoute.put("/like", like);
likeRoute.delete("/unLike", unLike);

module.exports = likeRoute;
