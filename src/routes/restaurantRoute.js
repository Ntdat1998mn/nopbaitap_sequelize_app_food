const express = require("express");
const restaurantRoute = express.Router();

// import commonjs Module
const { getRestaurant } = require("../controller/restaurantController");
const { getRestaurantId } = require("../controller/restaurantController");
const { createRestaurant } = require("../controller/restaurantController");
const { updateRestaurant } = require("../controller/restaurantController");

// Tao API phuong thuc GET
restaurantRoute.get("/getRestaurant", getRestaurant);
restaurantRoute.get("/getRestaurantId/:id", getRestaurantId);

// Create method POST
restaurantRoute.post("/createRestaurant", createRestaurant);
// Create method PUT
restaurantRoute.put("/updateRestaurant/:id", updateRestaurant);

module.exports = restaurantRoute;
