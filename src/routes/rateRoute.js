const express = require("express");
const likeRoute = express.Router();

// import commonjs Module
const { rate } = require("../controller/rateController");

// Tao API handleLike
likeRoute.put("/rate", rate);

module.exports = likeRoute;
