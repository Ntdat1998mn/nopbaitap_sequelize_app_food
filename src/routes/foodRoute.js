// localhost/api/user/version/method

const express = require("express");
const foodRoute = express.Router();

// import commonjs Module
const { getFood } = require("../controller/foodController");
/* const { getfoodId } = require("../controller/foodController");
const { createfood } = require("../controller/foodController");
const { updatefood } = require("../controller/foodController"); */

// Tao API phuong thuc GET
foodRoute.get("/getFood", getFood);
/* foodRoute.get("/getfoodId/:id", getfoodId);

// Create method POST
foodRoute.post("/createfood", createfood);
// Create method PUT
foodRoute.put("/updatefood/:id", updatefood);
 */
module.exports = foodRoute;
