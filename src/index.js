const express = require("express");
const app = express();
app.use(express.json());
const cors = require("cors");
app.use(cors());
app.listen(8081);
const rootRoute = require("./routes/rootRoute");
app.use("/api", rootRoute);
const mysql = require("mysql2");

const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "1234",
  port: 3306,
  database: "baittap_app_food",
});
