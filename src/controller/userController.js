const sequelize = require("../models/index");
const initModles = require("../models/init-models");
const model = initModles(sequelize);

const getUser = async (req, res) => {
  try {
    await model.user.findAll();

    res.status(200).send("Lấy dữ liệu thành công!");
  } catch (err) {
    res.status(500).send("Lỗi back end!");
  }
};
const getUserId = async (req, res) => {
  try {
    let { id } = req.params;
    let dataOne = await model.user.findOne({ where: { user_id: id } });

    if (dataOne) {
      res.status(200).send("Lấy user thành công!");
    } else {
      res.status(400).send("User không tồn tại!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end");
  }
};
const createUser = async (req, res) => {
  try {
    let { full_name, email, password } = req.body;
    // Them data vao co so du lieu

    let data = await model.user.create({
      full_name,
      email,
      password,
    });

    if (data) {
      res.status(200).send("Thêm User thành công!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end!");
  }
};

const updateUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataOne = await model.user.findOne({ where: { user_id: id } });

    if (dataOne) {
      let { full_name, email, password } = req.body;

      let data = {
        full_name,
        email,
        password,
      };
      await model.user.update(data, { where: { user_id: id } });

      res.status(200).send("Cập nhật user thành công!");
    } else {
      res.status(400).send("User không tồn tại!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end");
  }
};

module.exports = {
  getUser,
  getUserId,
  createUser,
  updateUser,
};
