const sequelize = require("../models/index");
const initModles = require("../models/init-models");
const model = initModles(sequelize);

const like = async (req, res) => {
  try {
    let { user_id, res_id, date_like } = req.body;
    let data = await model.like_res.findAll({
      where: { user_id, res_id },
    });

    if (data.length == 0) {
      await model.like_res.create({ user_id, res_id, date_like });
      res.status(200).send("Like thành công!");
    } else {
      /*   await model.like_res.destroy({
        where: { user_id, res_id },
      }); */
      res.status(200).send("Đã like nhà hàng!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end");
  }
};

const unLike = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let data = await model.like_res.findAll({
      where: { user_id, res_id },
    });
    if (data.length !== 0) {
      await model.like_res.destroy({
        where: { user_id, res_id },
      });
      res.status(200).send("Unlike thành công!");
    } else {
      res.status(200).send("Đã unLike nhà hàng!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end");
  }
};
module.exports = { like, unLike };
