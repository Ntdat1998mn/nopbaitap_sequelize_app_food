const sequelize = require("../models/index");
const initModles = require("../models/init-models");
const model = initModles(sequelize);

const rate = async (req, res) => {
  try {
    let { user_id, res_id, amount, date_rate } = req.body;
    let data = await model.rate_res.findAll({
      where: { user_id, res_id },
    });
    if (data.length == 0) {
      await model.rate_res.create({ user_id, res_id, amount, date_rate });
      res.status(200).send("Rate thành công!");
    } else {
      await model.rate_res.update(
        { user_id, res_id, amount, date_rate },
        { where: { user_id, res_id } }
      );
      res.status(200).send("Update rate thành công!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end");
  }
};

module.exports = { rate };
