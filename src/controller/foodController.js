const sequelize = require("../models/index");
const initModles = require("../models/init-models");
const model = initModles(sequelize);

const getFood = async (req, res) => {
  // let data = await model.food.findAll({
  //   include: ["sub_foods"],
  // });
  let data = await model.food.findAll();
  res.send(data);
};

module.exports = { getFood };
