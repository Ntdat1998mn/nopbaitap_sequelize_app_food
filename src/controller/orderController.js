const sequelize = require("../models/index");
const initModles = require("../models/init-models");
const model = initModles(sequelize);

const createOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount, arr_sub_id } = req.body;
    let data = await model.order.findAll({ where: { user_id, food_id } });

    if (data.length == 0) {
      await model.order.create({
        user_id,
        food_id,
        amount,
        arr_sub_id,
      });
      res.status(200).send("Đặt món ăn thành công!");
    } else {
      res.status(200).send("Đã đặt món!");
    }
  } catch (err) {
    res.status(500).send("Lỗi back end!");
  }
};

module.exports = { createOrder };
